
#### Intro

This project  is written in typescript and uses react-native 0.61.1 and redux-thunk

To get transactions details I used amberdata.io API as it has a websockets API alongside a REST API, since we would want a transaction confirmation as soon as possible I went with it. (from my testing it gets the transaction confirmation faster than etherscan website)

When a transaction is successfull it shows a message saying it was successful, if the app is in the background it shows a push notification

To be easier to test, I also added the funcionality to get a random pending transaction then wait for it's confirmation

Used third-party packages for the loading of contacts, and notifications.

Forked  react-native-contacts-wrapper (https://github.com/filipef101/react-native-contacts-wrapper) to suport react native 0.60+

Used xCode 10.1


#### How to run 

- clone or download repo
- node version v12.5.0, lower might work but wasn't tested
- for ios you need cocoapods installed
- android needs build tools 28.0.3


```js
yarn install
cd ios
pod install
yarn ios or yarn android
```
