import { Platform, PermissionsAndroid, Rationale } from 'react-native';
import ContactsWrapper from 'react-native-contacts-wrapper';

export const loadContact = async () => {

    try {
        if (Platform.OS === 'android') {
            await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
                {
                    'title': 'Contacts',
                    'message': 'This app would like to view your contacts.'
                } as Rationale
            )
        }
        const contact = await ContactsWrapper.getContact()

        return contact
    } catch (error) {
        return null
    }

};
