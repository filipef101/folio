import store from '../store/index'
import { transactionResultAction, verifyHashAction } from '../store/actions/index'
import axios from 'axios'
import { Alert } from 'react-native'

const baseURL = 'https://web3api.io/api/v2/' // TODO should go to a config file
const headers = { 'x-api-key': 'UAK1ca1f3453e5cdd8ac4ce1a2aceb395dd' }  // TODO sensitive

export const verifyRandomPendingHash = async () => {
    const res = await axios({
        method: 'get',
        url: baseURL + 'transactions/?status=pending&size=1',
        headers
    })

    if (res.data && res.data.payload && res.data.payload.records && res.data.payload.records[0]) {
        store.dispatch(verifyHashAction(res.data.payload.records[0].hash))

        return
    } else {
        return null
    }
}

const getTransaction = async (hash) => {
    return axios({
        method: 'get',
        url: baseURL + 'transactions/' + hash,
        headers
    })
}

let ws: WebSocket

const onmessage = (e) => {
    // a message was received
    if (!e || !e.data) return
    let data = null
    try {
        data = JSON.parse(e.data)
    } catch (error) {
        return
    }

    if (data && data.params && data.params.result && data.params.result.blockHash) {
        const status = data.params.result.status
        const hash = data.params.result.blockHash
        let success = false
        if (status === '0x1' || status === '0x01') success = true
        store.dispatch(transactionResultAction(hash, success))

    }
};

const onerror = (e) => {
    // an error occurred
    console.log(e.message);
};

const onclose = (e) => {
    // connection closed
    console.log(e.code, e.reason);
};
function validate_txhash(addr) {
    return /^0x([A-Fa-f0-9]{64})$/.test(addr);
}
export const subscribeTransaction = (hash) => {

    if (!validate_txhash(hash)) {
        Alert.alert('', 'Please provide a valid hash')
        return false
    }

    getTransaction(hash).then(res => { // try to see if transactions is already verified
        console.log('getTransaction')
        console.log(res)

        if (res && res.status === 200 && res.data && res.data.payload && res.data.payload.status === '0x1') { // if transaction is already verified
            store.dispatch(transactionResultAction(hash, true))

        } else if (res && res.status === 200 && res.data && res.data) {
            // subscibe via websockets for success
            ws.send('{"jsonrpc":"2.0","id":2,"method":"subscribe","params":["transaction",{"hash":"' + hash + '"}]}'); // send a message
        } else if (res && res.status === 200 && res.data && res.data) {
            // FIX : the transaction is too recent and amberdata api doesn't return yet the transaction data, so fetch it again
            setTimeout(() => {
                subscribeTransaction(hash)
            }, 1500);
            return
        } else {
            console.log('99')
            store.dispatch(verifyHashAction(null))
            Alert.alert('', 'Could not verify hash, ensure it is correct or try again')
        }
    }).catch(e => { throw (e) })

}

export const initAmber = async () => {
    console.log('starting amberdata websocket')
    return new Promise((resolve, reject) => {
        try {
            ws = new WebSocket('wss://ws.web3api.io?x-api-key=UAK1ca1f3453e5cdd8ac4ce1a2aceb395dd');
            ws.onopen = () => {
                // connection opened
                console.log('Connection opened')
                resolve()
            };
            ws.addEventListener('message', onmessage)
            // ws.onmessage = onmessage
            ws.onerror = onerror
            ws.onclose = onclose

        } catch (error) {
            reject(error)
        }
    })

}
