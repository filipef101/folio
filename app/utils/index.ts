import { string } from 'prop-types';
import PushNotification from 'react-native-push-notification';
import { Platform, AppState, Alert } from 'react-native';

export const createAsyncDelay = duration => {

    return new Promise((resolve, _) => setTimeout(() => { resolve(); }, duration));
};

export const showNotification = (notification: { body: string, title?: string }) => {
    if (AppState.currentState === 'active') {
        Alert.alert(
            notification.title || '',
            notification.body
        )
        return
    }
    PushNotification.localNotification({
        message: notification.body,
        title: notification.title
    })
}
