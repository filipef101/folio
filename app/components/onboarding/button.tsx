import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TextInputProps,
    TextProps,
    StyleProp,
    ViewStyle
} from 'react-native'
import React, { Component, useEffect, useState, Ref } from 'react';
import { iOSUIKit } from 'react-native-typography'

type presets = 'main' | 'clear'

type Props = TextInputProps & {
    onPress?: () => void
    textStyle?: TextProps
    style?: StyleProp<ViewStyle>
    text?: string,
    preset?: presets
};

const Input = (props: Props) => {

    return (
        <View style={props.style}>
            <TouchableOpacity style={props.preset ? styles[props.preset] : styles.main} onPress={props.onPress}>
                <Text style={props.textStyle || styles.text} >{props.text}</Text>
            </TouchableOpacity>
        </View>

    )
};
export default Input;
const styles = StyleSheet.create({
    main: {
        backgroundColor: 'rgb(248,88,44)',

        height: 50,
        width: '100%',
        maxWidth: 550,
        borderRadius: 50,
        alignContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'

    },
    clear: {
        height: 50,
        width: '100%',
        maxWidth: 550,
        borderRadius: 50,
        alignContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: 'rgba(255,255,255,0.3)'
    },
    text: {
        textAlign: 'center',
        marginLeft: 20,
        flex: 1,
        marginRight: 20,
        ...iOSUIKit.subheadEmphasizedWhiteObject,
        fontSize: 14
    }
})
