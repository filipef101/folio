import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    ActivityIndicator,
    ImageBackground,
    StatusBar,
    TextInputProps,
    TouchableWithoutFeedback
} from 'react-native'
import React, { Component, useEffect, useState, Ref } from 'react';
import Icons from '../../theme/icons/index'

type Props = TextInputProps & {
    icon?: keyof typeof Icons
    handleRef?: (ref: TextInput) => void
    id: string
};

const refs: { [key: string]: TextInput } = {}
const press = (id) => {
    if (refs[id]) refs[id].focus()
}
const Input = (props: Props) => {

    const Logo = props.icon && Icons[props.icon]
    const handleRef = (r: TextInput) => {
        if (!r) return
        if (props.handleRef) {
            props.handleRef(r)
        }
        refs[props.id] = r

    }

    return (
        <View style={props.style} >
            <View style={styles.bg} />
            <TouchableWithoutFeedback onPress={() => press(props.id)}>
                <View style={styles.main}>
                    {Logo && <Logo style={styles.logo} />}
                    <TextInput ref={handleRef} placeholderTextColor='rgba(255,255,255,0.62)' {...props} style={styles.textInput} />
                </View>
            </TouchableWithoutFeedback>
        </View>

    )
};

export default Input;
const styles = StyleSheet.create({
    bg: {
        height: 50,
        width: '100%',
        maxWidth: 550,
        borderRadius: 50,
        backgroundColor: 'white',
        opacity: 0.1,
        position: 'absolute'
    },
    main: {
        height: 50,
        width: '100%',
        maxWidth: 550,
        borderRadius: 50,
        // backgroundColor: 'grey',
        // opacity: 0.1,
        alignContent: 'center',
        alignItems: 'center',
        // justifyContent: 'center',
        flexDirection: 'row'
    },
    logo: {
        opacity: 1,
        // marginRight: 25,
        height: 20,
        marginLeft: 20,

        width: 20
        // backgroundColor: 'black',
        // marginLeft: 17
    },
    textInput: {
        padding: 10,
        paddingLeft: 20,
        fontSize: 13,
        color: 'white',
        flex: 1,
        paddingRight: 20
    },
})