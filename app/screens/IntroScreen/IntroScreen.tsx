import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    ActivityIndicator,
    ImageBackground,
    StatusBar,
    SafeAreaView,
    TouchableWithoutFeedback,
    Keyboard,
    KeyboardAvoidingView,
    ScrollView,
    Dimensions,
    LayoutAnimation
} from 'react-native'
import React, { Component, useEffect, useState, useRef } from 'react';
import LinearGradient from 'react-native-linear-gradient';
import { useSelector, useDispatch } from 'react-redux'
import { MaterialColors, Typography } from '../../theme'
import { initAmber, verifyRandomPendingHash } from '../../services/amberdata'
import styles from './styles'
import { loadContactAction, verifyHashAction } from '../../store/actions'
import { initialState } from '../../store/reducers'
import { useNavigation } from 'react-navigation-hooks'
import Input from '../../components/onboarding/input';
import Button from '../../components/onboarding/button';
// import { ScrollView } from 'react-native-gesture-handler';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { iOSUIKit } from 'react-native-typography';
import { LoginButton } from 'react-native-fbsdk';
const IntroScreen = () => {

    let ref: null | TextInput = null

    const handleRef = (r: TextInput) => {
        ref = r
    }
    const change = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        setCurrType(currType !== 'register' ? 'register' : 'login')
    }
    const [currType, setCurrType] = useState<'register' | 'login'>('register')
    const [email, setEmail] = useState<string>('')
    const [pw, setPw] = useState<string>('')
    const onLogin = () => {

    }
    const onRegister = () => {

    }
    return (
        <LinearGradient colors={['#ff6a3a', '#000000']} style={{ flex: 1 }}>
            {/* <TouchableWithoutFeedback style={{ flex: 1 }} onPress={Keyboard.dismiss} > */}
            <SafeAreaView style={{ flex: 1 }}>
                <KeyboardAwareScrollView bounces={false} contentContainerStyle={{ ...styles.main, flexGrow: 1, flex: 0 }}>
                    {/* <KeyboardAvoidingView style={[styles.inputs, { height: 500 }]}> */}
                    <View style={styles.innerView}>
                        <Text style={[iOSUIKit.largeTitleEmphasizedWhite, { paddingBottom: 10 }]}>Welcome</Text>
                        <Text style={[iOSUIKit.subheadWhite, styles.introText]}>This is a portefolio app developed in React Native to showcase my skills</Text>
                    </View>
                    <View key={currType !== 'register' ? 'register' : 'login'} style={styles.innerView}>
                        <View style={styles.inputs}>
                            <Input value={email} onChangeText={setEmail} id='email' icon={'Email'} placeholder={'Email'} returnKeyType={'next'} autoCompleteType={'email'} style={styles.input} onSubmitEditing={() => ref && ref.focus()} />
                            <Input value={pw} onChangeText={setPw} id='password' handleRef={handleRef} icon={'Lock'} placeholder={'Password'} secureTextEntry={true} autoCompleteType={'password'} style={styles.input} />
                        </View>
                        <Button onPress={currType === 'register' ? onRegister : onLogin} style={styles.loginButton} text={currType === 'register' ? 'Register' : 'Login'} />
                    </View>
                    <View style={styles.innerView}>
                        <View style={styles.innerViewBottom}>
                            <TouchableOpacity>
                                <Text style={[iOSUIKit.calloutWhite, { padding: 20 }]}>Forgot Password</Text>
                            </TouchableOpacity>

                            <TouchableOpacity key={currType !== 'register' ? 'register' : 'login'} onPress={change} >
                                <Text style={[iOSUIKit.calloutWhite, { padding: 20 }]}>{currType !== 'register' ? 'Register' : 'Login'}</Text>
                            </TouchableOpacity>

                        </View>
                        <Button preset='clear' onPress={() => { }} style={styles.loginButton} text={'Continue as guest'} />

                        <LoginButton style={{
                            // backgroundColor: 'rgb(248,88,44)',
                            marginTop: 5,
                            height: 49,
                            width: '80%',
                            maxWidth: 550,
                            borderRadius: 46 / 2,
                            // alignContent: 'center',
                            // alignItems: 'center',
                            // flexDirection: 'row',
                            overflow: 'hidden'
                        }} />

                    </View>
                    {/* </KeyboardAvoidingView> */}
                </KeyboardAwareScrollView>
            </SafeAreaView>
            {/* </TouchableWithoutFeedback> */}
        </LinearGradient>
    )

}

export default IntroScreen
