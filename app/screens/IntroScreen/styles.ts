import {
    StyleSheet
} from 'react-native'
import { MaterialColors, Typography } from '../../theme'

export default StyleSheet.create({ // TODO styles should go to a separate file and refractor styles in jsx
    innerView: {
        alignContent: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
    },
    innerViewBottom: {
        flexDirection: 'row',
        alignContent: 'center',
        justifyContent: 'space-between',
        // marginRight: '5%',
        // marginLeft: '5%',
        alignItems: 'center',
        maxWidth: 580,
        width: '80%',
    },
    main: {
        flex: 1,
        justifyContent: 'space-around',
        alignItems: 'center',
        alignContent: 'center',
        // justifyContent: 'center',
        // alignItems: 'center'
        // backgroundColor: 'red'

    },
    input: {
        width: '80%',
        marginBottom: 9,
        marginTop: 9,
        alignSelf: 'center',
        // backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'center',
    },
    loginButton: {
        width: '80%',
        marginBottom: 9,
        marginTop: 9,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
    },
    inputs: {
        width: '100%',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    },
    welcome: {
        fontSize: 36,
        fontWeight: 'bold'
    },
    introText: {
        opacity: 0.5,
        width: '80%',
        maxWidth: 550,
        textAlign: 'center',
        // marginBottom: '10%'
    }
})
