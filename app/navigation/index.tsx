import { createStackNavigator } from 'react-navigation-stack';
import { IntroScreen, TransactionsScreen } from '../screens';
import React from 'react'
import { createAppContainer } from 'react-navigation';
import analytics from '@react-native-firebase/analytics';
import Reactotron from 'reactotron-react-native'
// import { AppEventsLogger } from "react-native-fbsdk";
// gets the current screen from navigation state
function getActiveRouteName(navigationState) {
    if (!navigationState) {
        return null;
    }
    const route = navigationState.routes[navigationState.index];
    // dive into nested navigators
    if (route.routes) {
        return getActiveRouteName(route);
    }
    return route.routeName;
}
const AppRouteConfigs = {
    IntroScreen: {
        screen: IntroScreen
    },
    TransactionsScreen: {
        screen: TransactionsScreen
    }
}
const AppNavigator = createStackNavigator(AppRouteConfigs, { headerMode: 'none' });
const AppContainer = createAppContainer(AppNavigator);

export default () => (
    <AppContainer
        onNavigationStateChange={(prevState, currentState, action) => {
            const currentRouteName = getActiveRouteName(currentState);
            const previousRouteName = getActiveRouteName(prevState);
            if (previousRouteName !== currentRouteName) {
                // the line below uses the @react-native-firebase/analytics tracker
                // change the tracker here to use other Mobile analytics SDK.
                // tslint:disable-next-line: no-floating-promises
                analytics().setCurrentScreen(currentRouteName, currentRouteName);
                // AppEventsLogger.
                if (__DEV__) Reactotron.log('nav', currentRouteName)
            }
        }}
    />
);