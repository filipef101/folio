import { applyMiddleware, combineReducers, createStore, compose } from 'redux';
import thunk from 'redux-thunk';
import Reducer from './reducers';
import Reactotron from './../config/reactotronConfig'
// could have done a reducer for the contacts and another for the transactions but don't think is needed
const Reducers = {
    appData: Reducer
};
const Store = createStore(combineReducers(Reducers), compose(applyMiddleware(thunk), Reactotron.createEnhancer()));

export default Store;
