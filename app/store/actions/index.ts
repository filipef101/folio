import { TRANSACTIONRESULT_TYPE, LOADCONTACT_TYPE, TRANSACTIONSTART_TYPE } from './types';
import { PermissionsAndroid, Platform, Rationale, Alert } from 'react-native';
import { loadContact } from '../../services/contacts'
import { subscribeTransaction } from '../../services/amberdata';
import { showNotification } from '../../utils';

// could have done a separate actions file for contacts logic and transactions but don't think is needed since is a challenge/demo

export const verifyHashAction = (hash: string | null) => {
    if (hash) {
        const subs = subscribeTransaction(hash)
        if (subs === false) {
            return {
                type: TRANSACTIONSTART_TYPE,
                hash: null
            };
        }
    }
    return {
        type: TRANSACTIONSTART_TYPE,
        hash
    };
}

export const transactionResultAction = (id: string, result: boolean) => {
    if (result) {
        showNotification({ body: 'Transaction successful' })
    }
    return {
        type: TRANSACTIONRESULT_TYPE,
        result,
        id
    };
};

export const loadContactAction = () => {
    return (dispatch) => {
        loadContact().then(contact => {
            dispatch({
                type: LOADCONTACT_TYPE,
                contact
            });
        }).catch(() => { })
    };
}

// export const loadContactAction = async () => {
