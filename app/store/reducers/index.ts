import { TRANSACTIONRESULT_TYPE, LOADCONTACT_TYPE, TRANSACTIONSTART_TYPE } from '../actions/types';
import { act } from 'react-test-renderer';
export type initialState = {
    transactions: { [key: string]: boolean },
    loadedContact: null | { email?: string, phone?: string, name?: string },
    currentTransaction: null | string;
}
export const initialState: initialState = {
    transactions: {},
    loadedContact: null,
    currentTransaction: null
};

const Reducer = (state = initialState, action) => {
    console.log('ACTION: ' + action.type)
    console.log(action)
    console.log('ACTION: ' + action.type)

    switch (action.type) {

        case TRANSACTIONRESULT_TYPE:
            return {
                ...state,
                ...{ transactions: { ...state.transactions, ...{ [action.id]: action.result } }, currentTransaction: null }
            };

        case TRANSACTIONSTART_TYPE:
            return {
                ...state,
                ...{ transactions: state.transactions, currentTransaction: action.hash }
            };

        case LOADCONTACT_TYPE:
            return {
                ...state,
                ...{ loadedContact: action.contact }
            };

        default:
            return state;
    };
};

export default Reducer;
